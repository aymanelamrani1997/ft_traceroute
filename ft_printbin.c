/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printbin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aelamran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/01 10:00:24 by aelamran          #+#    #+#             */
/*   Updated: 2019/02/01 10:00:46 by aelamran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

void	ft_printbin(unsigned char c, int start, int end)
{
	int nb;

	while (start >= end)
    {
		nb = (c >> start) & 1;
		printf("%d", nb);
		start--;
    }
}
