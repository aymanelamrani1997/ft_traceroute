/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_traceroute.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aelamran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/31 17:45:12 by aelamran          #+#    #+#             */
/*   Updated: 2019/03/13 13:51:30 by aelamran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "libft.h"
#include <errno.h>
#include <string.h>

void	ft_printpacket(char *data, int ret);
void	ft_analysepacket(char *buf, int ret);
void	ft_printbin(unsigned char c, int start, int end);

typedef struct		s_ping
{
	unsigned char	type;
	unsigned char	code;
	unsigned short	id;
	unsigned short	sequence_num;
	unsigned short	header_checksum;
	char			data[8];
}					t_ping;

char	*ft_getaddr(char *buf)
{
	char	*c1;
	char 	*c2;
	char 	*c3;
	char 	*c4;
	char			*str;

	str = malloc(sizeof(unsigned char) * 5);
	c1 = ft_itoa((unsigned char)buf[12]);
	c2 = ft_itoa((unsigned char)buf[13]);
	c3 = ft_itoa((unsigned char)buf[14]);
	c4 = ft_itoa((unsigned char)buf[15]);
	str = ft_strjoin(c1, ".");
	str = ft_strjoin(str, c2);
	str = ft_strjoin(str, ".");
	str = ft_strjoin(str, c3);
	str = ft_strjoin(str, ".");
	str = ft_strjoin(str, c4);
	return (str);
}

int	main(int ac, char **av)
{
	struct sockaddr_in	src_addr;
	struct sockaddr_in	dst_addr;
	t_ping				ping;
	char				buf[500];
	char				*addr_dest;
	int					ttl;
	unsigned int		len;
	int					sockfd;
	int					ret;

	ttl = 1;
	addr_dest = NULL;
	if ((sockfd = socket(AF_INET, SOCK_DGRAM, 1)) != -1)
	{
		src_addr.sin_family = AF_INET;
		src_addr.sin_port = htons(1);
		src_addr.sin_addr.s_addr = INADDR_ANY;

		dst_addr.sin_family = AF_INET;
		dst_addr.sin_port = htons(1);
		inet_aton(av[1], &dst_addr.sin_addr);
		if ((ret = bind(sockfd, (const struct sockaddr *) &src_addr, sizeof(src_addr))) != -1)
		{
			ping.type = 8;
			ping.code = 0;
			ping.id = 1337;
			ping.sequence_num = 0;
			ping.header_checksum = 0x1f82;
			strcpy(ping.data, "ayman");
			while (1)
			{
				setsockopt(sockfd, IPPROTO_IP, IP_TTL, &ttl, sizeof(ttl));
				if (sendto(sockfd, &ping, sizeof(ping), 0, (const struct sockaddr *) &dst_addr, sizeof(dst_addr)) > 0)
				{
					if ((ret = recvfrom(sockfd, (char *)buf, 500, MSG_WAITALL, (struct sockaddr *) &dst_addr, &len)) > 0)
					{
						buf[ret] = '\0';
						addr_dest = ft_getaddr(buf);
						printf("%s\n", addr_dest);
					}
				}
				if (ft_strcmp(av[1], addr_dest) == 0)
					break ;
				ttl++;
			}
		}
		else
			perror("bind");
	}
	else
		perror("socket");
	return (0);
}
