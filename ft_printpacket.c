/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printpacket.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aelamran <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/30 13:11:00 by aelamran          #+#    #+#             */
/*   Updated: 2019/01/30 13:11:23 by aelamran         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

void	ft_printpacket(char *data, int ret)
{
	int i;
	char str[17];
	int j;
	int count;

	i = 0;
	j = 0;
	count = 0;
	while (i < ret)
    {
		printf("%.2hhx", data[i]);
		if (data[i] >= 33 && data[i] <= 127)
			str[j++] = data[i];
		else
			str[j++] = '.';
		if (i % 16 != 0)
		{
			if (i % 8 == 0)
				printf("   ");
			else if (i + 1 % 8 != 0)
				printf(" ");
		}
		if (i % 16 == 0)
		{
			str[j] = '\0';
			printf("%*s\n", 49 - (count * 2), str);
			j = 0;
			count = 0;
		}
		i++;
		count++;
    }
	str[j] = '\0';
	count--;
	printf("%*s\n", 50 - (count * 2), str);
}
